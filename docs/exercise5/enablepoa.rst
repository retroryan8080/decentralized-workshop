Enabling the Proof of Authority Chain
=====================================

Most of the code for Proof of Authority was included in the base project and just needs to be enabled.

The code encompasses the 2 main parts of creating a Proof of Authority chain.  The first is deciding who is leading the authority round and broadcasting the newly created blocks.  The second part is broadcasting transactions that will be included in the next block.

These different parts of Proof of Authority are refered to here as part of the authority round or as part of the transaction broadcasting.

1. The biggest change with the gRPC protocol is that instead of having a single transaction for every block, each block now needs to have a list of transactions that are added to a block.  Update the definition of a block to have a list of transactions:

.. code-block:: console

  //modify the block definition to have a repeating list of transactions
  message Block {
      int32 index = 10;
      string creator = 15;
      string hash = 20;
      string previousHash = 30;
      google.protobuf.Timestamp timestamp = 40;
      repeated Transaction txn = 50;
  }

2. The gRPC protocol already includes the services for broadcasting transactions and proposing to lead Authority Rounds:

.. code-block:: console

    //These definitions are used for the Proof of Authority blockchain
    rpc broadcastTransaction(Transaction) returns (google.protobuf.Empty);
    rpc propose(ProposeRequest) returns (ProposeResponse);

3. Recompile the project to regenerate the gRPC classes.  The compilation will fail!


4.  In `BasicChain`, comment out the lines that get and set the transaction since we are not using the basic chain anymore.

Specifically, in the method `generateNextBlock` to comment out setTxn:

.. code-block:: console

  //.setTxn(nextTransaction)


5. In `metochi/client/src/main/java/metochi/grpc/BroadcastServiceImpl.java` review the Proof of Authority blockchain code and uncomment the override in the bottom methods to be sure the match the interface.  These are the server side implementation of proposing PoA and broadcasting new transactions to the network.

.. code-block:: console

  //These methods are used when creating a Proof of Authority blockchain
  @Override
  public void propose(metochi.ProposeRequest request,
                      io.grpc.stub.StreamObserver<metochi.ProposeResponse> responseObserver) {
    ...
  }

  @Override
  public void broadcastTransaction(metochi.Transaction request,
                                     io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
    ...
  }

6. The client side implementatins of PoA are in `metochi/client/src/main/java/metochi/grpc/BroadcastPeer.java` and `metochi/client/src/main/java/metochi/grpc/PeersManager.java`

7. In `metochi/client/src/main/java/metochi/grpc/ProofOfAuthorityChain.java`.

In that class uncomment and fix every section that is labeled with `//TODO POA`

Search the entire file for that comment and fix or uncomment the refrences to transactions:

.. code-block:: console

  //TODO POA - Add Genesis Block
  Block block = Block.newBuilder()
          .setIndex(0)
          .setCreator(nodeName)
          .setHash("816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f7d7")
          .setPreviousHash("0")
          .setTimestamp(BlockChainManager.getNowTimestamp())
          .addTxn(genesis_block)
          .build();

8. In `metochi/client/src/main/java/metochi/grpc/MetochiClient.java` the blockchain needs to be switched to using the Proof of Authority Chain:

In the initBlockchainManager change to using the PoA chain:

.. code-block:: console

        // TODO POA - switch to using the ProofOfAuthorityChain
        BlockChainManager blockChainManager = new ProofOfAuthorityChain(peersManager, nodeName, nodeURL)

9. In `metochi/client/src/main/java/metochi/grpc/MetochiClient.java` the authority node now needs to be started:

Uncomment the following code in the `main` method:

.. code-block:: console

        if (config.isAuthorityNode) {
            AuthorityNode authorityNode = new AuthorityNode(nodeName);
            authorityNode.start();
            optAuthorityNode = Optional.of(authorityNode);
        }

And then in that same class in the `readPrompt` method change from sending the message to broadcasting the message:

.. code-block:: console

  //in the first phase just send this message locally
  //later during Proof of Authority change this to broadcast the message
  // TODO POA - switch to using broadcastMsg
  //this was sendMsg(line)
  broadcastMsg(line);
