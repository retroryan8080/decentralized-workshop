Review Maven Configuration
==========================

Add the Maven dependencies
--------------------------

In `client/pom.xml`, there are the gRPC dependencies configured:

.. code-block:: console

  <project>
      <dependencies>
          <dependency>
              <groupId>io.grpc</groupId>
              <artifactId>grpc-netty</artifactId>
          </dependency>
          <dependency>
              <groupId>io.grpc</groupId>
              <artifactId>grpc-protobuf</artifactId>
          </dependency>
          <dependency>
              <groupId>io.grpc</groupId>
              <artifactId>grpc-stub</artifactId>
          </dependency>

      </dependencies>

  </project>



Note: In this example, the gRPC versions are specified in the parent POM’s `dependencyManagement` section.

Configure the plugins
---------------------

There are 2 plugins you must configure in order to automatically translate the proto file into Java code. First is the `os-maven-plugin`. This plugin will automatically detect the operation system and architecture of where the build is taking place, and makes the value available in `${os.detected.classifier}` variable. This is needed for the second plugin, `protobuf-maven-plugin` to download the native protoc generator. Native protoc generator is needed to translate proto file into Java code (or any other supported languages).

.. code-block:: console

  <project>
      <dependencies>
          ...
      </dependencies>
      ...
      <build>
          <extensions>
              <extension>
                  <groupId>kr.motd.maven</groupId>
                  <artifactId>os-maven-plugin</artifactId>
              </extension>
          </extensions>
          <plugins>
              <plugin>
                  <groupId>org.xolstice.maven.plugins</groupId>
                  <artifactId>protobuf-maven-plugin</artifactId>
              </plugin>
              ...
          </plugin>
      </build>
  </project>


**Note:** The plugins are also managed by the parent POM, in its pluginManagement section. There is much more to the configuration of protobuf-maven-plugin.  In addition to the plugin configuration to download the right version of the protoc generator, it also ties the generation into Maven’s compile and compile-custom phases, so that the files are auto-generated whenever you recompile your code.  See the parent POM for detail.

There is also a Gradle plugin!
