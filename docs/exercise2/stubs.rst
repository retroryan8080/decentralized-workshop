Generate Stubs
==============

Once you have the proto file and the Maven plugin configured, generate the Java class files from Maven

.. code-block:: console

  cd client/
  mvn compile

All Protobuffer 3 messages gRPC stubs should be generated under target/generated-sources:

.. code-block:: console

  find target/generated-sources
