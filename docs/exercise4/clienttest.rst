Testing from the Client
=======================

Let’s test it from a client.

Open `metochi/client/src/main/java/metochi/BroadcastPeer.java`

Implement the Init Broadcast Service
------------------------------------

1. First, implement `initBroadcastService()` method:

Use a `ManagedChannelBuilder` to create a new `ManagedChannel` and assign it to `authChannel`:

.. code-block:: console

  //TODO Initialize the Broadcast Channel and Broadcast Service here
  ManagedChannel broadcastChannel = ManagedChannelBuilder.forTarget(peerURL)
                  .usePlaintext(true).build();

2. Instantiate a new blocking stub and assign it to `broadcastService`:

.. code-block:: console

  // TODO Get a new Blocking Stub
  broadcastService = BroadcastServiceGrpc.newBlockingStub(broadcastChannel);

Implement `broadcast(...)`
--------------------------

This method will be called when you create a new block and want to broadcast it to the other nodes in the cluster:

1. Call `broadcastService.broadcast(...)` to broadcast the block

.. code-block:: console

  //TODO Broadcast a block here
  broadcastService.broadcast(BroadcastMessage
          .newBuilder()
          .setBlock(block)
          .setSender(senderURL)
          .build());
          
Implement `queryLatest(...)`
----------------------------

2. Implement queryLatest to get the latest block from other nodes.

.. code-block:: console

  //TODO Query for the latest block here
  return broadcastService.queryLatest(Empty.newBuilder().build());


Implement `queryAll(...)`
--------------------------

2. Implement queryAll to get the etnrie blockchain from other nodes.

.. code-block:: console

  //TODO  Query for the entire blockchain here
  return broadcastService.queryAll(Empty.newBuilder().build());

Enable the connection setup to other peers or nodes
---------------------------------------------------

In `metochi/client/src/main/java/metochi/MetochiClient.java` uncomment the line to init peers:

.. code-block:: console

  // TODO - uncomment init peers to connect this node to other nodes in the network
  client.initPeers();

`Hint: Full implementation here <https://github.com/retroryan/metochi/blob/exercise3/client/src/main/java/metochi/BroadcastPeer.java>`_
