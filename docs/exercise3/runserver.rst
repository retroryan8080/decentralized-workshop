Run the Server
==============

The project has exec-maven-plugin configured to run the main class.  Also be sure the `NODE_NAME` is set for every instance of the server.  This node name is used to load a unique configuration from the conf directory.

To run the server:

.. code-block:: console

  $ cd client
  $ export NODE_NAME=one
  $ mvn compile exec:java
  ...
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  ->

The log output is written to a log file based on the node name.  So node one there should be a `one.log` file.

.. code-block:: console

  $ more one.log
  [INFO ] 09:39:41.969 [main()] metochi.MetochiClient - loading config file: conf/node_one.conf
  [INFO ] 09:39:41.971 [main()] metochi.MetochiClient - starting with config: Config{port=9092, peerUrls=[localhost:9095, localhost:9097]}
  [INFO ] 09:39:42.654 [main()] metochi.MetochiClient - Server Started on port: 9092


Not bad for the first gRPC service!
