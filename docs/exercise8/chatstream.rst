Implement Chat Stream Service
=============================

Open chat-service/src/main/java/com/example/chat/grpc/ChatStreamServiceImpl.java. ChatStreamServiceImpl.chat(...) is the bidirectional stream stub we need to implement.

On the server side, we'll need to listen to incoming streamed messages. To do this, the server needs to return a StreamObserver, to listen to incoming messages:

.. code-block:: console

  @Override
  public StreamObserver<ChatMessage> chat(
    StreamObserver<ChatMessageFromServer> responseObserver) {
    final String username = Constant.USER_ID_CTX_KEY.get();

    return new StreamObserver<ChatMessage>() {
      @Override
      public void onNext(ChatMessage chatMessage) {

      }

      @Override
      public void onError(Throwable throwable) {

      }

      @Override
      public void onCompleted() {

      }
    };
  }

The responseObserver in the method parameter is what the server needs to use to stream data to the client.

The ChatMessage being streamed to the server may have different types, such as JOIN a room, LEAVE a room, or simply a TEXT message for a room:
1. When joining a chat room, add the responseObserver to the room's set of all currently connected observers.

.. code-block:: console

  @Override
  public void onNext(ChatMessage chatMessage) {
    Set<StreamObserver<ChatMessageFromServer>> observers =
        getRoomObservers(chatMessage.getRoomName());
    switch (chatMessage.getType()) {
      case JOIN:
        observers.add(responseObserver);
        break;
    }
  }

2. When leaving a chat room, remove the responseObserver from the room's observers set.

.. code-block:: console

  @Override
  public void onNext(ChatMessage chatMessage) {
    Set<StreamObserver<ChatMessageFromServer>> observers =
        getRoomObservers(chatMessage.getRoomName());
    switch (chatMessage.getType()) {
      case JOIN:
        observers.add(responseObserver);
        break;
      case LEAVE:
        observers.remove(responseObserver);
        break;
    }
  }

3. When sending a message, first make sure user is in the room, and then send to all the connected observers in the room:

.. code-block:: console

  @Override
  public void onNext(ChatMessage chatMessage) {
    Set<StreamObserver<ChatMessageFromServer>> observers =
        getRoomObservers(chatMessage.getRoomName());
    switch (chatMessage.getType()) {
      case JOIN:
        observers.add(responseObserver);
        break;
      case LEAVE:
        observers.remove(responseObserver);
        break;
      case TEXT:
        if (!observers.contains(responseObserver)) {
          responseObserver.onError(
            Status.PERMISSION_DENIED.withDescription("You are not in the room " +
              chatMessage.getRoomName()).asRuntimeException());
          return;
        }
        Timestamp now = Timestamp.newBuilder()
            .setSeconds(new Date().getTime()).build();
        ChatMessageFromServer messageFromServer =
            ChatMessageFromServer.newBuilder()
                .setType(chatMessage.getType())
                .setTimestamp(now)
                .setFrom(username)
                .setMessage(chatMessage.getMessage())
                .setRoomName(chatMessage.getRoomName())
            .build();
        observers.stream().forEach(o -> o.onNext(messageFromServer));
        break;
    }
  }

4. If there is an error, or when the client closes connection, remove the responseObserver from all rooms

.. code-block:: console

  @Override
  public void onError(Throwable throwable) {
      logger.log(Level.SEVERE, "gRPC error", throwable);
      removeObserverFromAllRooms(responseObserver);
  }

  @Override
  public void onCompleted() {
    removeObserverFromAllRooms(responseObserver);
  }

5. Run the authserver and chatserver in separate terminals:

.. code-block:: console

  //start auth server
  $ cd auth-service
  $ mvn install exec:java
  ...
  INFO: Server started on port 9091

  //start chat server
  $ cd chat-service
  $ mvn install exec:java
