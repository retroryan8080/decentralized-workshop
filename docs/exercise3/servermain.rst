Write the Server main entry
===========================

In `metochi/client/src/main/java/metochi/MetochiClient.java`’s `initServer()` method:

1. Use `ServerBuilder` to build a new server that listens on port 9091
2. Add an instance of `BroadcastServiceImpl` as a service: `builder.addService(...)`

.. code-block:: console

  // TODO Use ServerBuilder to create a new Server instance. Start it, and await termination.
  BroadcastServiceImpl broadcastService = new BroadcastServiceImpl(blockChainManager, optionalAuthorityNode);
  final Server server = ServerBuilder.forPort(config.port)
        .addService(broadcastService)
        .build();

3. In the same method add a shutdown hook to shutdown the server in case the process receives shutdown signal. This will try to shutdown the server gracefully, if shutdown hook is called.

.. code-block:: console

  Runtime.getRuntime().addShutdownHook(new Thread() {
    @Override
    public void run() {
      server.shutdownNow();
    }
  });

4. Finally in the same method start the server:

.. code-block:: console

  server.start();
  logger.info("Server Started on port: " + config.port);

Server will be running in a background thread. In this example we are running a command line prompt which prevents termination.

If you need to block on the server a call can be made to server.awaitTermination() to block the main thread

.. code-block:: console

  //NOT USED - example only
  server.awaitTermination();
