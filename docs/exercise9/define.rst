Define a Reporter and Tracer
============================

To use a tracer, first define a reporter so that trace data can be forwarded to a Zipkin endpoint. We'll use the URLConnectionSender, and post it to a local Zipkin instance.

You'll need to add this to every serverice and client:

* AuthServer.main(...)
* ChatServer.main(...)
* ChatClient

.. code-block:: console

  AsyncReporter<Span> reporter = AsyncReporter.create(
      URLConnectionSender.create("http://localhost:9411/api/v1/spans"));

  GrpcTracing tracing = GrpcTracing.create(Tracing.newBuilder()
      .localServiceName("my-service") // MAKE SURE YOU CHANGE THE NAME
      .reporter(reporter)
      .build());

Add Server Interceptors
-----------------------

We looked at server interceptors in the previous section.  Add the tracing.newServerInterceptor() to every service:

* AuthServer.main(...)
* ChatServer.main(...)

.. code-block:: console

  final Server server = ServerBuilder.forPort(9091)
          .addService(ServerInterceptors.intercept(
              someServiceImpl, tracing.newServerInterceptor()))
          .build();

Add Client Interceptor
----------------------

Also, add tracing.newClientInterceptor() to every outbound channels:

* ChatServer.main(...)
* ChatClient.initAuthService()
* ChatClient.initChatServices()

.. code-block:: console

  channel = ManagedChannelBuilder…
      .intercept(tracing.newClientInterceptor())
      .build()

Start Zipkin Server
-------------------

A Zipkin server JAR is already in the repository, under the zipkin-server directory. Start the local Zipkin server:

.. code-block:: console

  $ cd zipkin-server
  $ java -jar zipkin.jar

If you covered everything, restart all the servers and client, login and send a few more messages.  Then login into the Zipkin server to see the traces! (Access Zipkin console on http://localhost:9411)
