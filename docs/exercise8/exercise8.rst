Exercise 8 - Bidirectional Streaming
====================================

This exercise has not been updated for the decentralized workshops.  Sample code is available in metochi exercise 8.

The true power of gRPC lies in its use of HTTP/2 as the underlying protocol as oppose to HTTP/1. In HTTP/2, streaming is a native concept, and you can stream from server to client, from client to server, and also in both directions. Moreover, multiple streams are multiplexed over the same connection to reduce TCP connection handshake overhead.

In this lab, we'll use bidirectional streaming to establish a chat session with the server.


.. toctree::
  :maxdepth: 2

  chatstream
  chatcli
