Tracing
=======

In auth-service/pom.xml, you can see the dependencies:
1. Brave instrumentation library provides the client side and server side interceptors to propagate trace ID and Spans.
2. URL Connection Reporter can post trace data to a Zipkin server

.. code-block:: console

   <!-- Tracing -->
  <dependency>
    <groupId>io.zipkin.brave</groupId>
    <artifactId>brave-instrumentation-grpc</artifactId>
    <version>4.4.0</version>
  </dependency>
  <dependency>
    <groupId>io.zipkin.reporter</groupId>
    <artifactId>zipkin-sender-urlconnection</artifactId>
    <version>0.10.0</version>
  </dependency>
