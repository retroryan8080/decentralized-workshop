$(document).ready(function() {

    var allELm = []

    $(".codeset").each(function(index, el) {
        var c = $("<div class='codesnippet-widgets'><span class='current'>Java</span><span>Scala</span></div>");
        var javaButton = c.children()[0];
        var scalaButton = c.children()[1];
        javaButton.onclick = function() {
            //setDisplayJava(el,scalaButton, javaButton)
            updateAllEL("java")
            storeLangPref("java")
        };
        scalaButton.onclick = function() {
            //setDisplayScala(el,scalaButton, javaButton)
            updateAllEL("scala")
            storeLangPref("scala")
        };

        if ($(el).children(".highlight-java").length == 0) {
            // No Java for this example.
            javaButton.style.display = "none";
        }
        c.insertBefore(el);

        var langElm = [el, javaButton, scalaButton];
        allELm.push(langElm);
    });

    updateLang();

    function updateAllEL(lang) {
      for (var i = 0; i < allELm.length; i++) {

        var langElm = allELm[i];

        var el = langElm[0];
        var javaButton = langElm[1];
        var scalaButton = langElm[2];

        if (lang === "java") {
          setDisplayJava(el,javaButton, scalaButton)
        }
        else {
          setDisplayScala(el,javaButton, scalaButton)
        }
      }
    }

    function storeLangPref(displayLang) {
      // Check browser support
      if (typeof(Storage) !== "undefined") {
        // Store
        localStorage.setItem("favoriteLang", displayLang);
      }
    }

    function updateLang() {
      // Check browser support
      if (typeof(Storage) !== "undefined") {
        // Retrieve
        var favoriteLang = localStorage.getItem("favoriteLang");
        updateAllEL(favoriteLang);
      }
    }
});



function setDisplayJava(el, javaButton, scalaButton) {
  $(el).children(".highlight-java")[0].style.display = "block";
  $(el).children(".highlight-scala")[0].style.display = "none";
  scalaButton.setAttribute("class", "");
  javaButton.setAttribute("class", "current");
}

function setDisplayScala(el, javaButton, scalaButton) {
  $(el).children(".highlight-java")[0].style.display = "none";
  $(el).children(".highlight-scala")[0].style.display = "block";
  javaButton.setAttribute("class", "");
  scalaButton.setAttribute("class", "current");
}
