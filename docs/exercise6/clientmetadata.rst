Passing Metadata from a Client
==============================

Decorate with MetadataUtils
---------------------------

In `BroadcastPeer.initBroadcastService()`, we instantiated the `broadcastService` stub. We can decorate the stub using `MetadataUtils` to attach additional headers:

.. code-block:: console

  public void initBroadcastService() {
    …
    Metadata metadata = new Metadata();
    metadata.put(Constant.JWT_METADATA_KEY, jwtToken);

    broadcastService = MetadataUtils.attachHeaders(
            BroadcastServiceGrpc.newBlockingStub(broadcastChannel), metadata);
  }

Now, every outgoing call from `broadcastService` will have the JWT metadata attached.

Let's try it by the three nodes and watching what happens as blocks get broadcast:

.. code-block:: console

  $ cd client
  $ mvn compile exec:java
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  ->
  Token: eyJ0eXAiOiJKV1QiLCJhbGci...

Every call you should see the JWT token printed in the console:

Metadata Client Interceptor
---------------------------

Alternatively, you can use a client interceptor to attach headers too. There is an out of the box interceptor to do just that. In `BroadcastPeer.initBroadcastService`, use the stub normally. But you can add the client interceptor to the Channel:

.. code-block:: console

    public void initBroadcastService() {
    ...
    Metadata metadata = new Metadata();
    metadata.put(Constant.JWT_METADATA_KEY, token);

    // TODO Add JWT Token via a Call Credential
    broadcastChannel = ManagedChannelBuilder.forTarget(peerURL)
      .intercept(MetadataUtils.newAttachHeadersInterceptor(metadata))
      .usePlaintext(true)
      .build();

    broadcastService = BroadcastServiceGrpc.newBlockingStub(broadcastChannel);
