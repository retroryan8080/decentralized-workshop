Define the Metadata
===================

All Metadata is strongly typed and keyed. That means, rather than using a `String` as a key you pass into a `Map`, you have to create a Metadata Key.

Open `client/src/main/java/metochi/jwt/Constant.java` to see the metadata constants for transferring JWT token between services:

.. code-block:: console

  public static final Metadata.Key<String> JWT_METADATA_KEY =
    Metadata.Key.of("jwt", ASCII_STRING_MARSHALLER);


Using Interceptors to pass Metadata
-----------------------------------

Capture Metadata from Server Interceptor
----------------------------------------

On the server side, metadata can only be captured from a server interceptor. Open `client/src/main/java/metochi/jwt/JwtServerInterceptor.java`

Capture the JWT token and print it out:

.. code-block:: console

  public class JwtServerInterceptor implements ServerInterceptor {
    ...

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
      // TODO Get token from Metadata
      String token = metadata.get(Constant.JWT_METADATA_KEY);
      System.out.println("Token: " + token);

      return serverCallHandler.startCall(serverCall, metadata);
    }
  }

Attach Server Interceptor
-------------------------

Open `client/src/main/java/metochi/MetochiClient.java`. The JwtServerInterceptor needs to be instantiated. Then we need to add it into the interceptor chain:

In `initServer` method, we used `ServerBuilder` to register existing service instances. Modify it to apply interceptors for each of the service:

.. code-block:: console

  // TODO Add JWT Server Interceptor, then later, trace interceptor
  BroadcastServiceImpl broadcastService = new BroadcastServiceImpl(blockChainManager, optionalAuthorityNode);
  final JwtServerInterceptor jwtServerInterceptor = new JwtServerInterceptor(Constant.ISSUER, Algorithm.HMAC256("secret"));

  final Server server = ServerBuilder.forPort(config.port)
          .addService(ServerInterceptors
                  .intercept(broadcastService, jwtServerInterceptor))
          .build();

Now, every request sent to these services will first be intercepted by the JWTServerInterceptor.

compile the code to date but don't run it until we implement the client:

.. code-block:: console

  $ mvn compile
