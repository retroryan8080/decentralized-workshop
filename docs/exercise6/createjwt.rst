Creating a JWT
===============

We need to create a JWT token for every node.  What should happen is this node would go through an authentication process at startup to determine if it is really an authority node and assign a JWT claim based on that.
For simplicity we fake it by setting it in a config file.

In `client/src/main/java/metochi/jwt/JwtAuthService.java` we need to implement the method the generates the JWT token:

.. code-block:: console

  protected String generateToken(String nodeName, boolean isAuthorityNode) {
      //TODO generate jwt token with a claim for being an authority node or not
      return JWT.create()
              .withIssuer(Constant.ISSUER)
              .withSubject(nodeName)
              .withClaim(IS_AUTHORITY, isAuthorityNode)
              .sign(algorithm);
  }

The token is created with a claim as to whether this node is an authority node.

Then in `client/src/main/java/metochi/MetochiClient.java`, in the `main` method, create a token a pass it to the Peers Manager:

.. code-block:: console

  //TODO Create and pass a JWT to Peers Manager
  //This manages the list of peer nodes that this node connects to
  String token = JwtAuthService.instance().authenticate(nodeName, config.isAuthorityNode);
  logger.info("jwt token:" + token);
  PeersManager peersManager = new PeersManager(token)
