Exercise 4 - Testing the Client
===============================

In this exercise we will test the server by implementing the stubs in the client

.. toctree::
  :maxdepth: 2

  clienttest
  clientrun
