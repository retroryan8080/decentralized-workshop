Exercise 9 - Tracing
====================

THIS EXERCISE WAS NOT UPDATED FOR THE BLOCKCHAIN SAMPLE!! It applies to older exercises!

There are a number of OpenTracing tracers for gRPC. In this lab, we'll use Brave. Brave has a number of Java-based trace interceptors that can propagate trace headers across network boundaries. Similarly, there is one for gRPC!



.. toctree::
  :maxdepth: 2

  tracing
  define
