Enforce Role-based Access
=========================

Now that we have the JWT Token with the `IS_AUTHORITY` Claim in Context, how can we access it from the server stubs? For example, to get the `IS_AUTHORITY`, and check if a node is an authority node?

Currently, anyone can fake being an authority node and propose an authority round . Let's lock it down so that only authority nodes can propose.  In `BroadcastServiceImpl.failBecauseNoAdminRole(...)` add the following:

1. Get the Decoded JWT token from context

.. code-block:: console

  protected <T> boolean failBecauseNotAuthorityNode(StreamObserver<T> responseObserver) {
    // TODO Retrieve JWT from Constant.JWT_CTX_KEY
    DecodedJWT jwt = Constant.JWT_CTX_KEY.get();
  }


2. Use it to fetch the associated roles, and then validate if the user has the admin role

.. code-block:: console

  protected <T> boolean failBecauseNotAuthorityNode(StreamObserver<T> responseObserver) {
     // TODO Retrieve JWT from Constant.JWT_CTX_KEY
     DecodedJWT jwt = Constant.JWT_CTX_KEY.get();
     Claim claim = jwt.getClaim(Constant.IS_AUTHORITY);
     logger.info("failBecauseNotAuthorityNode claim: " + claim);
     if ((!claim.isNull()) && (claim.asBoolean())) {
         logger.error("failing call because not authority node");
         StatusRuntimeException isNotAnAuthorityNode
                 = new StatusRuntimeException(Status.PERMISSION_DENIED.withDescription("Not an authority node!"));
         responseObserver.onError(isNotAnAuthorityNode);
         return true;
     } else {
         return false;
     }
 }

Simulate a rouge authority node
-------------------------------

To simulate a rouge authority node proposing an authority round have every node start an authority node.

In `MetochiClient.main(...)` change the authority node to be the following code block:

.. code-block:: console

  Optional<AuthorityNode> optionalAuthorityNode = Optional.empty();
   AuthorityNode authorityNode = new AuthorityNode(nodeName, blockChainManager, peersManager);
   authorityNode.start();

   if (config.isAuthorityNode) {
       optionalAuthorityNode = Optional.of(authorityNode);
   }

Let's try it by running three nodes and watching what happens as authority rounds get broadcast.  What should happen is you see the following error appearing on some nodes:

.. code-block:: console

  $ cd client
  $ mvn compile exec:java
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  -> [WARNING]
  io.grpc.StatusRuntimeException: PERMISSION_DENIED: Not an authority node!

What is happening is the non authority node is being denied from proposing to run an authority round.

Return things to a functioning state!
-------------------------------------

Be sure to return the authority node code back to the original state:

.. code-block:: console

  Optional<AuthorityNode> optionalAuthorityNode = Optional.empty();

   if (config.isAuthorityNode) {
     AuthorityNode authorityNode = new AuthorityNode(nodeName, blockChainManager, peersManager);
     authorityNode.start();
     optionalAuthorityNode = Optional.of(authorityNode);
   }
