Review the Proto File
=====================

First, define message payloads and RPC operations for the brodcast service. We’ll have three RPC operations:

* **Broadcast**

  :Input: Block to be broadcast
  :Output: Response to the broadcast (not used).


* **QueryLatest**

  :Input: Empty
  :Output: Returns the latest block from the node

* **QueryAll**

    :Input: Empty
    :Output: Returns the entire blockchain from the node

Initialize the Proto file
-------------------------

Open the proto file: `client/src/main/proto/MetochiProto.proto`

  1. See that the Protobuffer syntax to proto3 to ensure we are using Protobuffer 3 syntax.
  2. Specify the package. The package corresponds to the Java packages.
  3. Set protoc options with option keyword. By default, the generator will generate all of the message classes into a single giant class file. In this example, we’ll generate each of the Protobuffer message into its own class by setting the `java_multiple_files` option.

.. code-block:: console

  // 1. Review the syntax, package, and options
  syntax = "proto3";
  option java_multiple_files = true;
  package metochi;

Define messages
---------------

After defining the syntax, package, and options, define the message payloads. Notice that every field is strongly typed. There is a variety of out of the box primitives:

* Primitives such as `string`, `int32`, `int64`, `bool`, etc.
* To represent an array, use `repeated`, e.g., `repeated string`
* Strongly typed map/hash, e.g., `map<string, int64>`
* Custom types, such as

  * Nested types
  * Re-use defined types
  * Enumerations, e.g. `enum Sentiment { HAPPY = 0; SAD = 1; }`

Every field must be assigned a tag number (e.g., string username = 1). The tag number is what uniquely identifies the field rather than the name of the field. When serialized, the tag number is stored in the serialized payload and transferred across the wire.

.. code-block:: console

  syntax = "proto3";
  ...

  // 2. Review the message definitions
  message Transaction {
      string uuid = 10;
      string message = 20;
      string sender = 30;
  }

  message Block {
      int32 index = 10;
      string creator = 15;
      string hash = 20;
      string previousHash = 30;
      google.protobuf.Timestamp timestamp = 40;
      Transaction txn = 50;
  }

  message Blockchain {
      repeated Block chain = 10;
  }

  message BroadcastResponse {
      string response = 10;
  }

Define service and operations
-----------------------------

After defining the message payload, define the service, and operations within the service. Every operation must have an input type and an output type.

For the `BroadcastService` service, we’ll define an unary request, which is basically a simple request/response:

.. code-block:: console

  syntax = "proto3";
  ...

  // 3. Review the service definitions
  service BroadcastService {
    rpc Broadcast (Block) returns (BroadcastResponse);
    rpc QueryLatest(google.protobuf.Empty) returns (Block);
    rpc QueryAll(google.protobuf.Empty) returns (Blockchain);
  }
