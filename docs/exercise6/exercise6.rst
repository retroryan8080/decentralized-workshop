Exercise 6 - Metadata and Interceptors
================================================

You can use Metadata to pass additional request information, such as an authentication token, or a trace ID, across network boundary to another service.  Effectively, all metadata key/value pairs are transmitted via HTTP/2 headers. There is no direct access to HTTP/2. Metadata is the abstraction for referring to the header.

.. toctree::
  :maxdepth: 2

  createjwt
  metadata
  clientmetadata
