Setup
=====

This is the Java Version of the workshop.

Pre-Requisites
--------------

* `OpenJDK 8 <http://openjdk.java.net/install/>`_
* `Maven 3+ <https://maven.apache.org/install.html>`_
* A Good Java IDE - IntelliJ or Eclipse
* We’ll be writing Java code!

Getting the Code
----------------

The code is on GitHub, and checkout the initial branch:

.. code-block:: console

  git clone git@github.com:retroryan/metochi.git

Compile
-------

Make sure everything compiles out of the box!

.. code-block:: console

  cd metochi
  mvn compile

Setup the environment variables
-------------------------------

Every node in the cluster needs to have a unique node name envrionment variable of one, two, three or four.  This is done by exporting the `NODE_NAME` environment variable.  This node name is used to load the configuration for the node from the conf directory.

.. code-block:: console

  $ export NODE_NAME=one


Test the sample application
---------------------------

.. code-block:: console

  $ cd client
  $ mvn compile exec:java
  ...

At the command line commands are entered by starting with a /
Everything else is considered a message.  For example try the following at the command line:

.. code-block:: console

  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  -> Hello World
  Generated block: index: 1
  creator: "one"
  hash: "8a1f1e03c54d6ae4a5a33fc8d38090215ce1254dc8f31c0e685299d9fdb48ecc"
  previousHash: "816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f7d7"
  timestamp {
    seconds: 1523598853
    nanos: 989000000
  }
  txn {
    uuid: "7d94aae8-728b-44cd-9504-cbffe8eb1ae7"
    message: "Hello World"
    sender: "one"
  }

  message | /blocks | /addpeer [data] | /getpeers | /quit
  -> /b
  blockchain written to file: one0.blocks
  message | /blocks | /addpeer [data] | /getpeers | /quit
  -> /q



IntelliJ Setup
--------------
Import the top-level `mitochi/pom.xml` as a Maven Project. This should also import the sub-modules as well.

The latest versions of IntelliJ automatically add generated source folders to your project, so this step will not be needed if you are running a newer version of IntelliJ.
If you find IntelliJ is not recognizing your generated source folders try the following:

To add the generted gRPC source files to your project.

1 - After compiling the project verify the protobuf generated sources are under target/generated-sources/protobuf.

2 - Then under the project settings update the auth-service project.

3 - Click on sources and then on the target path navigate down to target/generated-sources/protobuf.

4 - Add both grpc-java and java as source folders.

