Exercise 5 - Proof of Authority (PoA)
=====================================


This is designed to be a simple example of how to build a Proof of Authority (PoA) blockchain with gRPC. It is not production worthy and has significant holes in the design. It does a lot of anti-patterns like blocking calls to simplify the design. Metochi uses a very naive Proof of Authority mechanism to establish which nodes are the authority nodes. If a node in the config file has isAuthorityNode equal to true than it is considered an authority.

Future improvements would use a more sophisticated approach to establish authority and tie that to a nodes identity.

The consensus algorithm used is very simplisitic for this sample. A more complete example of a consensus algorithm would be to use the `Tendermint Byzantine Consensus Algorithm <http://tendermint.readthedocs.io/en/master/introduction.html#consensus-overview>`_

This example takes a very simplistic approach of running authority rounds approxiamtely every 10 seconds. Each authority node proposes to lead a round approxiamtely every 10 secs. with a small random offset to create the next block. The other nodes either accept or reject this proposal, depending on if they are already proposing a vote.

That is the process by which blocks are created.  The second part of PoA is including transactions in the block.  This is done by having the nodes keep a list of recent transactions broadcast by other nodes. When that nodes creates or "mines" a block it includes those transactions.  True Proof of Authority would do some type of verification of those transactions, such as signature validation.

.. toctree::
  :maxdepth: 2

  enablepoa
