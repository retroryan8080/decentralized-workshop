Exercise 3 - Implement Stubs
============================

In this exercise we will implement the stubs defined in the protobuf file

.. toctree::
  :maxdepth: 2

  serverstub
  servermain
  runserver
