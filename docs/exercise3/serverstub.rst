Implement Server Stub
=====================

Open `metochi/client/src/main/java/metochi/grpc/BroadcastServiceImpl.java`. In this file, we need to:

  1. Extend from the gRPC generated BroadcastServiceImplBase class
  2. Implement the broadcast, queryLatest and queryAll methods

Extend the Base Implementation
------------------------------

.. code-block:: console

  ...
  // TODO Extend gRPC's BroadcastServiceImplBase
  public class BroadcastServiceImpl extends BroadcastServiceGrpc.BroadcastServiceImplBase  {
    ...
  }

Override and Implement the broadcast, queryLatest and queryAll Methods
----------------------------------------------------------------------

The base implementation bootstraps the underlying descriptors and pipes necessary for gRPC server to communicate with the actual service implementation, and makes the call to the actual service methods.  The service methods have default implementations in the base class, but they will all throw Status.UNIMPLEMENTED error.  You’ll need to override these methods explicitly:

Override broadcast
------------------

Override the broadcast() base implementation method

.. code-block:: console

  //TODO - Override the broadcast, queryLatest and queryAll methods here

  /**
   *
   * This handles a call to broadcast a block to this node.  When the call is received this node add the block to it's blockchain.
   *
   * @param request
   * @param responseObserver
   */
  @Override
  public void broadcast(metochi.BroadcastMessage request,
                        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      logger.info("server broadcast received block index: " + request.getBlock().getIndex());
      blockChainManager.addLatestBlock(request.getBlock(), request.getSender());
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
  }


Override queryLatest and queryAll
---------------------------------

Override the queryLatest and queryAll method:

.. code-block:: console

  /**
   *
   * This handles a call to query the latest block from this node and sends it back to the node that made the query.
   *
   * @param request
   * @param responseObserver
   */
  @Override
  public void queryLatest(com.google.protobuf.Empty request,
                          io.grpc.stub.StreamObserver<metochi.Block> responseObserver) {
      try {
          logger.info("server queryLatest");
          responseObserver.onNext(blockChainManager.getLatestBlock());
          responseObserver.onCompleted();
      } catch (Exception e) {
          e.printStackTrace();
      }
  }


  /**
   *
   * This handles a call to query the entire blockchain from this node and sends it back to the node that made the query.
   *
   * @param request
   * @param responseObserver
   */
  @Override
  public void queryAll(com.google.protobuf.Empty request,
                       io.grpc.stub.StreamObserver<metochi.Blockchain> responseObserver) {
      try {
          logger.info("server queryAll");
          responseObserver.onNext(blockChainManager.getBlockchain());
          responseObserver.onCompleted();
      } catch (Exception e) {
          e.printStackTrace();
      }
  }

`Hint: Full implementation here <https://github.com/retroryan/metochi/blob/exercise3/client/src/main/java/metochi/grpc/BroadcastServiceImpl.java>`_
