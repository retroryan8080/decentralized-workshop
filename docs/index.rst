Welcome to Building Decentralized Applications with gRPC
========================================================

In this workshop, we'll be building a the basics of a blockchain with gRPC and Java. The blockchain is designed to be used as an immutable ledger of data. In this example it will be used as an immutable ledger of messages sent by nodes, but it could be used for any type of data.

The blockchain is designed to be deployed on a private network in a design of what is sometimes termed an enterprise or private blockchain rather than a public blockchain. To this end instead of using the traditional Proof of Work like bitcoin, it uses a Proof of Authentication mechanism to secure the blockchain.


Disclaimers
===========

This is really simplistic implementation that is not production worthy.  It does not fully address security, high availability, etc. In order to simplify the architecture the authority or validator nodes are defined in a configuration file, which is an obvious security flaw. Also to simplify things a predefined list of peer nodes are pre-defined for every node. A better architecture would be to do something like use a seed node to gather cluster information and randomly distributed the load across the cluster.


.. toctree::
  :maxdepth: 2

  setup/setup
  exercise1/exercise1
  exercise2/exercise2
  exercise3/exercise3
  exercise4/exercise4
  exercise5/exercise5
  exercise6/exercise6
  exercise7/exercise7
  exercise8/exercise8
  



Authors - Ryan Knight `@knight_cloud <https://twitter.com/knight_cloud>`_ and Ray Tsang `@saturnism <https://twitter.com/saturnism>`_

Based on the original `Workshop of Ray Tsang <http://bit.ly/grpc-java-lab-doc>`_
