Exercise 2 - Basics
===================

In this exercise, we’ll use gRPC to create a simple Authentication service.  Given a username, password pair, the authentication service will validate and if successful, return a JWT token.

.. toctree::
  :maxdepth: 2

  protofile
  maven
  stubs
