Exercise 7 - Context Propagation
================================

Metadata is great to propagate metadata information across network boundary. It's also text only, or that it'll be serialized into text. But it's not accessible from server stubs. What if the server stub needs to get the JWT token for validation? Or, what if you need to propagate certain values downstream to nested calls within the same JVM?

For that, we need to use Context.  Similar to Metadata, to use Context, we must declare the Context keys.

.. toctree::
  :maxdepth: 2

  propagation
  access
  clientcredential
