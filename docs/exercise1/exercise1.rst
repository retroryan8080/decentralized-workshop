Exercise 1 - Overview of Building a Blockchain
==============================================

The initial project consist of a very basic blockchain application that just runs as a local application.  In the next several exercises we will walk through making this a decentralized blockchain application.

The exercises will create a basic server which will be run on each node to broadcast new blocks as they are added to the blockchain.  Each node will also be a client that recieves new blocks and adds them to the local copy of the blockchain.

The workshop then takes the basic blockchain and adds in Proof of Authority as the second major part of the workshop.
Some sections of the code will be commented refering to Proof of Authority that will be used in later phases of the workshop.


A brief overview of the sample project:

Metochi Client - this is the main class and starts up a command line prompt.  In later exercises we will add the client and server initialization to this project.

Basic Chain - This is the basis for the blockchain.  It stores a copy of the blockchain, manages creating the genesis block and creates new blocks.
