Using Kubernetes Services
=========================

In this exercise we are going to modify the server URL's from being hard code to localhost to using ones injected by kubernetes.

This will allow the application to use Kubernetes Service names to discover the other microservices to call.

Setup the Environment File
--------------------------

This code will look for an environment variable for the host name or use a default.

Copy this file from git under the exercise 8 branch to the auth service project:

 `EnvVars <https://github.com/retroryan/grpc-java-chatroom-workshop/blob/solution/auth-service/src/main/java/com/example/auth/EnvVars.java>`_

Kubernetes Deployment Descriptors
---------------------------------

Be sure you have all the kubernetes deployment files in the kubernetes folder.  If they are missing copy them from this folder:

`kubernetes folder <https://github.com/retroryan/grpc-java-chatroom-workshop/tree/solution/kubernetes>`_


Update the Server URLs
----------------------

All of the URLs in the code base need to be updated to use the environment vars.

For example look at these version of Auth Server, Channel Manager and Chat Server to see how it uses the EnvVars:

`AuthServer <https://github.com/retroryan/grpc-java-chatroom-workshop/blob/solution/auth-service/src/main/java/com/example/auth/AuthServer.java>`_

`ChatClient <https://github.com/retroryan/grpc-java-chatroom-workshop/blob/solution/chat-cli-client/src/main/java/com/example/chat/ChatClient.java>`_

`ChatServer <https://github.com/retroryan/grpc-java-chatroom-workshop/blob/solution/chat-service/src/main/java/com/example/chat/ChatServer.java>`_


Update Database File Location
-----------------------------

Look at how the User Repository database file name has been updated in this file to use an environment variable:

`UserDatabase <https://github.com/retroryan/grpc-java-chatroom-workshop/blob/solution/auth-service/src/main/java/com/example/auth/repository/UserRepository.java>`_
