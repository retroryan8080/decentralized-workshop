Exercise 10 - Deployment to Kubernetes
======================================

THIS EXERCISE WAS NOT UPDATED FOR THE BLOCKCHAIN SAMPLE!! It applies to older exercises!

In this exercise we will deploy the chat service to Kubernetes.

To simplify access to the chat client command line that service is still run locally.




.. toctree::
  :maxdepth: 2

  usingservices
  deployment
