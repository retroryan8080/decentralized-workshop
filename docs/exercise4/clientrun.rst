Run the Client
==============

Open up multiple terminal windows to run other nodes in.  Then start up 3 nodes and you should see the blockchain being broadcast to other nodes.

Notice the configuration under the conf directory for nodes one, two and three point to each other.  If a node is not found it will cause a gRPC exception, so be sure to start all 3 nodes.

In the first terminal window run:

.. code-block:: console

  $ cd client
  $ mvn compile
  $ export NODE_NAME=one
  $ mvn exec:java
  ...
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  ->

In separate terminal window you can tail the output of the logfile:

.. code-block:: console

  $ tail -F one.log

In the second terminal window run:

.. code-block:: console

  $ cd client
  $ export NODE_NAME=two
  $ mvn exec:java
  ...
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  ->

In the third terminal window run:

.. code-block:: console

  $ cd client
  $ export NODE_NAME=three
  $ mvn exec:java
  ...
  [INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ metochi-client ---
  Press Ctrl+D or Ctrl+C to quit
  message | /blocks | /addpeer [data] | /getpeers | /quit
  ->

Then to test the application try the following:

1 - On the first terminal type a message:

.. code-block:: console

  ->Hello World

2 - On the second terminal type another message:

.. code-block:: console

  ->Hello gRPC

3 - On the third terminal window save the blockchain to a file:

.. code-block:: console

  ->/b
  blockchain written to file: three0.blocks

If you open up `three0.blocks` you will see all of the messages from the other nodes:
